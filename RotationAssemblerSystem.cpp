// Copyright (C) 2007-2015 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Garth N. Wells 2007-2009
// Modified by Ola Skavhaug 2007-2009
// Modified by Kent-Andre Mardal 2008
// Modified by Joachim B Haga 2012
// Modified by Martin Alnaes 2013-2015

#include <pybind11/pybind11.h>

#include <algorithm>

#include <Eigen/Dense>

#include <dolfin/log/log.h>
#include <dolfin/log/Progress.h>
#include <dolfin/common/Array.h>
#include <dolfin/common/Timer.h>
#include <dolfin/parameter/GlobalParameters.h>
#include <dolfin/la/GenericTensor.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Facet.h>
#include <dolfin/mesh/Vertex.h>
#include <dolfin/mesh/MeshData.h>
#include <dolfin/mesh/MeshFunction.h>
#include <dolfin/mesh/SubDomain.h>
#include <dolfin/function/GenericFunction.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/fem/Form.h>
#include <dolfin/fem/UFC.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/fem/DirichletBC.h>
//#include <dolfin/fem/OpenMpAssembler.h>
#include <dolfin/fem/AssemblerBase.h>

#include <dolfin/la/GenericMatrix.h>

namespace dolfin {

      /// This class provides automated assembly of linear systems, or
  /// more generally, assembly of a sparse tensor from a given
  /// variational form.
  ///
  /// Subdomains for cells and facets may be specified by assigning
  /// subdomain indicators specified by _MeshFunction_ to the _Form_
  /// being assembled:
  ///
  ///    .. code-block:: c++
  ///
  ///        form.dx = cell_domains
  ///        form.ds = exterior_facet_domains
  ///        form.dS = interior_facet_domains

class RotationAssemblerSystem : public AssemblerBase
{
public:

  /// Constructor
  RotationAssemblerSystem() {}

  /// Assemble tensor from given form
  ///
  /// *Arguments*
  ///     A (_GenericTensor_)
  ///         The tensor to assemble.
  ///     a (_Form_)
  ///         The form to assemble the tensor from.
  void assemble(GenericMatrix& A, 
                const Form& a,
                GenericVector& L, 
                const Form& b,
                const Function& rotation_function);

  /// Assemble tensor from given form over cells. This function is
  /// provided for users who wish to build a customized assembler.
  void assemble_cells(GenericMatrix& A, const Form& a, 
                      GenericVector& L, const Form& b, 
                      UFC& ufca,
                      UFC& ufcb,
                      std::shared_ptr<const MeshFunction<std::size_t>> domains,
                      std::vector<double>* values);

  /// Assemble tensor from given form over exterior facets. This
  /// function is provided for users who wish to build a customized
  /// assembler.
  void assemble_exterior_facets(GenericMatrix& A, const Form& a,
                                GenericVector& L, const Form& b,
                                UFC& ufca,
                                UFC& ufcb,
                                std::shared_ptr<const MeshFunction<std::size_t>> domains,
                                std::vector<double>* values);

  /// Assemble tensor from given form over interior facets. This
  /// function is provided for users who wish to build a customized
  /// assembler.
  void assemble_interior_facets(GenericMatrix& A, const Form& a,
                                GenericVector& L, const Form& b,
                                UFC& ufca,
                                UFC& ufcb,
                                std::shared_ptr<const MeshFunction<std::size_t>> domains,
                                std::shared_ptr<const MeshFunction<std::size_t>> cell_domains,
                                std::vector<double>* values);

  /// Assemble tensor from given form over vertices. This function is
  /// provided for users who wish to build a customized assembler.
  void assemble_vertices(GenericMatrix& A, const Form& a,
                         GenericVector& L, const Form& b,
                         UFC& ufca,
                         UFC& ufcb,
                         std::shared_ptr<const MeshFunction<std::size_t>> domains);

private:
  bool walk_coefficients(std::size_t& n, 
                         std::shared_ptr<const dolfin::FunctionSpace> func_space,
                         std::shared_ptr<const dolfin::FunctionSpace> rotation_space);

  std::size_t value_size(std::shared_ptr<const dolfin::FunctionSpace> func_space);
  
  struct rotation_coefficient_struct 
  {
    std::size_t coefficient_dimension = 0;
    std::size_t coef_idx = 0;
    std::size_t rotation_value_size = 0;
    std::size_t rotation_n_nodes = 0;
    std::size_t start_rotation_index;
  };
  rotation_coefficient_struct rot_coefs;
  rotation_coefficient_struct rot_args[2];

  void find_coef_rotation_indices(const std::vector<std::shared_ptr<const GenericFunction>>& coefficients,
                                  const Function& rotation_function);
  void find_rotation_indices(rotation_coefficient_struct & rot,
                             const std::size_t i,
                             const std::shared_ptr<const dolfin::FunctionSpace> func_space,
                             const Function& rotation_function);

};

//----------------------------------------------------------------------------
void RotationAssemblerSystem::find_coef_rotation_indices(
    const std::vector<std::shared_ptr<const GenericFunction>>& coefficients,
    const Function& rotation_function)
{//std::cerr << "XXX ";
  for (std::size_t i = 0; i < coefficients.size(); i++) 
  {
    const Function*const f = dynamic_cast<const Function*const>(coefficients[i].get());
    if (f != nullptr) {
      const std::shared_ptr<const dolfin::FunctionSpace> func_space = f->function_space();
      find_rotation_indices(rot_coefs, i, func_space, rotation_function);
    }
  }
}

void RotationAssemblerSystem::find_rotation_indices(
    rotation_coefficient_struct & rot, 
    const std::size_t i,
    const std::shared_ptr<const FunctionSpace> func_space,
    const Function& rotation_function)
{
  rot.start_rotation_index = 0;
  if (walk_coefficients(rot.start_rotation_index, func_space, rotation_function.function_space()))
  {
    rot.coefficient_dimension = func_space->element()->space_dimension();
    rot.coef_idx = i;
    rot.rotation_value_size = value_size(rotation_function.function_space());
    rot.rotation_n_nodes = rotation_function.function_space()->element()->space_dimension() / 
                                 rot.rotation_value_size;
//     std::cerr << "TROVATO!\n";
//     std::cerr << rot.start_rotation_index << " " << rot.coefficient_dimension << " " << rot.coef_idx 
//         << " " << rot.rotation_value_size << " " << rot.rotation_n_nodes << std::endl;
  }
}

std::size_t RotationAssemblerSystem::value_size(std::shared_ptr<const dolfin::FunctionSpace> func_space)
{
  std::size_t value_size = 1;
  for (std::size_t d = 0; d < func_space->element()->value_rank(); d++)
    value_size *= func_space->element()->value_dimension(d);
  return value_size;
};

bool RotationAssemblerSystem::walk_coefficients(std::size_t& n, 
                       std::shared_ptr<const dolfin::FunctionSpace> func_space, 
                       std::shared_ptr<const dolfin::FunctionSpace> rotation_space)
{
  bool found = false;
  for (std::size_t i = 0; i < func_space->element()->num_sub_elements(); i++) {
    if (func_space->sub(i) == rotation_space) {
        if (value_size(func_space->sub(i)) != 3)
          dolfin_error("RotationAssembler::walk_coefficients",
                                 "correctly identify the rotation (sub)space",
                                 "The size of rotation field is != 3: coef.sub(s_space).value_size() = %d != 3", value_size(func_space->sub(i)));
        found = true;
        break;
    } 
    else if (func_space->sub(i)->element()->num_sub_elements() > 0) 
    {
        found = walk_coefficients(n, func_space->sub(i), rotation_space);
    }
    else
    {
        n += func_space->sub(i)->dofmap()->max_element_dofs();
    }
  }
  return found;
};

void RotationAssemblerSystem::assemble(GenericMatrix& A, 
                                 const Form& a,
                                 GenericVector& L, 
                                 const Form& b,
                                 const Function& rotation_function)
{
  // All assembler functions above end up calling this function, which
  // in turn calls the assembler functions below to assemble over
  // cells, exterior and interior facets.

  // Get cell domains
  std::shared_ptr<const MeshFunction<std::size_t>>
    cell_domains = a.cell_domains();

  // Get exterior facet domains
  std::shared_ptr<const MeshFunction<std::size_t>> exterior_facet_domains
      = a.exterior_facet_domains();

  // Get interior facet domains
  std::shared_ptr<const MeshFunction<std::size_t>> interior_facet_domains
      = a.interior_facet_domains();

  // Get vertex domains
  std::shared_ptr<const MeshFunction<std::size_t>> vertex_domains
    = a.vertex_domains();

  // Check form
  AssemblerBase::check(a);
  AssemblerBase::check(b);

  // Create data structure for local assembly data
  UFC ufca(a);
  UFC ufcb(b);

  // Check ranks
  if (ufca.form.rank() != 2)
    dolfin_error("RotationAssembler::assemble",
                    "assemble",
                    "RotationAssembler works only with forms a of rank 2");
  if (ufcb.form.rank() != 1)
    dolfin_error("RotationAssembler::assemble",
                    "assemble",
                    "RotationAssembler works only with forms b of rank 1");

    // Update off-process coefficients
  const std::vector<std::shared_ptr<const GenericFunction>>
    coefficients = b.coefficients();

  // Find rotation (sub)space indices  
  find_coef_rotation_indices(coefficients, rotation_function);
  //std::cerr << "Rank = " << ufc.form.rank() << std::endl;
  for (std::size_t i = 0; i < ufca.form.rank(); ++i)
    find_rotation_indices(rot_args[i], i, a.function_space(i), rotation_function);

  // Initialize global tensor
  init_global_tensor(A, a);
  init_global_tensor(L, b);

  // Assemble over cells
  assemble_cells(A, a, L, b, ufca, ufcb, cell_domains, NULL);

  // Assemble over exterior facets
  assemble_exterior_facets(A, a, L, b, ufca, ufcb, exterior_facet_domains, NULL);

  // Assemble over interior facets
  assemble_interior_facets(A, a, L, b, ufca, ufcb, interior_facet_domains,
                           cell_domains, NULL);

  // Assemble over vertices
  assemble_vertices(A, a, L, b, ufca, ufcb, vertex_domains);

  // Finalize assembly of global tensor
  if (finalize_tensor) 
  {
    A.apply("add");
    L.apply("add");
  }
}
//-----------------------------------------------------------------------------
void RotationAssemblerSystem::assemble_cells(
  GenericMatrix& A,
  const Form& a,
  GenericVector& L,
  const Form& b,
  UFC& ufca,
  UFC& ufcb,
  std::shared_ptr<const MeshFunction<std::size_t>> domains,
  std::vector<double>* values)
{
  // Skip assembly if there are no cell integrals
  if (!(ufca.form.has_cell_integrals() || ufcb.form.has_cell_integrals()))
    return;

  // Set timer
  Timer timer("Assemble cells");

  // Extract mesh
  dolfin_assert(a.mesh());
  const Mesh& mesh = *(a.mesh());

  // Form rank
  const std::size_t form_rank = ufca.form.rank();

  // Collect pointers to dof maps
  std::vector<const GenericDofMap*> dofmaps;
  for (std::size_t i = 0; i < form_rank; ++i)
    dofmaps.push_back(a.function_space(i)->dofmap().get());

  // Vector to hold dof map for a cell
  std::vector<ArrayView<const dolfin::la_index>> dofs(form_rank);

  // Cell integral
  ufc::cell_integral* integrala = ufca.default_cell_integral.get();
  ufc::cell_integral* integralb = ufcb.default_cell_integral.get();

  // Check whether integral is domain-dependent
  bool use_domains = domains && !domains->empty();
  
  // Used to store cell switched nodes
  std::vector<double> cell_switched_nodes(rot_coefs.rotation_n_nodes);
  
  // Assemble over cells
  ufc::cell ufc_cell;
  std::vector<double> coordinate_dofs;
  
  Progress p(AssemblerBase::progress_message(A.rank(), "cells"),
             mesh.num_cells());
  for (CellIterator cell(mesh); !cell.end(); ++cell)
  {    
    // Get integral for sub domain (if any)
    if (use_domains)
    {
      integrala = ufca.get_cell_integral((*domains)[*cell]);
      integralb = ufcb.get_cell_integral((*domains)[*cell]);
    }
    
    // Skip if no integral on current domain
    if (!(integrala||integralb))
      continue;

    // Check that cell is not a ghost
    dolfin_assert(!cell->is_ghost());

    // Update to current cell
    cell->get_cell_data(ufc_cell);
    cell->get_coordinate_dofs(coordinate_dofs);
    ufca.update(*cell, coordinate_dofs, ufc_cell,
               integrala->enabled_coefficients());
    ufcb.update(*cell, coordinate_dofs, ufc_cell,
               integralb->enabled_coefficients());

    // Get local-to-global dof maps for cell
    bool empty_dofmap = false;
    for (std::size_t i = 0; i < form_rank; ++i)
    {
      auto dmap = dofmaps[i]->cell_dofs(cell->index());
      dofs[i] = ArrayView<const dolfin::la_index>(dmap.size(), dmap.data());
      empty_dofmap = empty_dofmap || dofs[i].size() == 0;
    }

    // Skip if at least one dofmap is empty
    if (empty_dofmap)
      continue;
    
    // If the cell has switched rotation dofs switch then ...
    bool switched_cell = false;
    double * wra = ufca.w()[rot_coefs.coef_idx] + rot_coefs.start_rotation_index;
    double * wrb = ufcb.w()[rot_coefs.coef_idx] + rot_coefs.start_rotation_index;
    Eigen::Vector3d save_phi[rot_coefs.rotation_n_nodes];
    bool switched_node[rot_coefs.rotation_n_nodes];
    switched_node[0] = false;
    constexpr double half_pi = std::acos(-1.) / 2.;
    constexpr double pi2 = 2. * std::acos(-1.);
    if (rot_coefs.rotation_value_size)
    {
      save_phi[0](0) = wra[0];
      save_phi[0](1) = wra[0 + rot_coefs.rotation_n_nodes];
      save_phi[0](2) = wra[0 + 2 * rot_coefs.rotation_n_nodes];
      if (std::sqrt(save_phi[0].dot(save_phi[0])) > half_pi)
      {
        for (std::size_t n = 1; n < rot_coefs.rotation_n_nodes; n++) 
        {
          Eigen::Vector3d& phi = *(save_phi + n);
          phi(0) = wra[n];
          phi(1) = wra[n + rot_coefs.rotation_n_nodes];
          phi(2) = wra[n + 2 * rot_coefs.rotation_n_nodes];
          switched_node[n] = (save_phi[0].dot(phi) < 0);
          if (switched_node[n])
          {
//             std::cerr << "Switched node " << n << "; cell " << cell->index() << std::endl;
            switched_cell = true;
            double normphi = std::sqrt(phi.dot(phi));
            double c = 1. - pi2 / normphi;
            wra[n] = phi(0) * c;
            wra[n + rot_coefs.rotation_n_nodes] = phi(1) * c;
            wra[n + 2 * rot_coefs.rotation_n_nodes] = phi(2) * c;
            wrb[n] = phi(0) * c;
            wrb[n + rot_coefs.rotation_n_nodes] = phi(1) * c;
            wrb[n + 2 * rot_coefs.rotation_n_nodes] = phi(2) * c;
          }
        }
      }
    }

    // Tabulate cell tensor
    integrala->tabulate_tensor(ufca.A.data(), ufca.w(),
                              coordinate_dofs.data(),
                              ufc_cell.orientation);
    integralb->tabulate_tensor(ufcb.A.data(), ufcb.w(),
                              coordinate_dofs.data(),
                              ufc_cell.orientation);

    // Transform the tabulated values
    if (switched_cell) {
      for (std::size_t n = 0; n < rot_coefs.rotation_n_nodes; n++) 
      {
        if (switched_node[n])
        {
          Eigen::Vector3d& phi = *(save_phi + n);
          const double normphi2 = phi.dot(phi);
          const double normphi = std::sqrt(normphi2);
          const double pi2_normphi3 = pi2 / normphi2 / normphi;
          const double c1 = 1. - pi2 / normphi;
        
          // Rank 2 on the right (trial function)
          if (rot_args[1].rotation_value_size)
          {              
            //Row Major
            for (std::size_t row = 0; row < dofs[0].size(); row++)
            {
              double * r = ufca.A.data() + row * dofs[1].size() + rot_args[1].start_rotation_index + n;
              const double c2 = (r[0] * phi(0) + 
                           r[0 + rot_coefs.rotation_n_nodes] * phi(1) + 
                           r[0 + 2 * rot_coefs.rotation_n_nodes] * phi(2)
                          ) * pi2_normphi3;
              r[0] = r[0] * c1 + phi(0) * c2;
              r[0 + rot_coefs.rotation_n_nodes] = r[0 + rot_coefs.rotation_n_nodes] * c1 + phi(1) * c2;
              r[0 + 2 * rot_coefs.rotation_n_nodes] = r[0 + 2 * rot_coefs.rotation_n_nodes] * c1 + phi(2) * c2;
            }
          }
          if (rot_args[0].rotation_value_size)
          {
            // Rank 2 on the left (test function)
            double * rb = ufcb.A.data() + rot_args[0].start_rotation_index + n;
            Eigen::Vector3d res(rb[0], rb[0 + rot_coefs.rotation_n_nodes], rb[0 + 2 * rot_coefs.rotation_n_nodes]);
            for (std::size_t col = 0; col < dofs[1].size(); col++)
            {
              double * r = ufca.A.data() + col + dofs[1].size() * (rot_args[0].start_rotation_index + n);

              double c2 = (r[0] * phi(0) + 
                           r[0 + dofs[1].size() * rot_coefs.rotation_n_nodes] * phi(1) + 
                           r[0 + 2 * dofs[1].size() * rot_coefs.rotation_n_nodes] * phi(2)
                          ) * pi2_normphi3;
              r[0] = r[0] * c1 + phi(0) * c2;
              r[0 + dofs[1].size() * rot_coefs.rotation_n_nodes] = 
                  r[0 + dofs[1].size() * rot_coefs.rotation_n_nodes] * c1 + phi(1) * c2;
              r[0 + 2 * dofs[1].size() * rot_coefs.rotation_n_nodes] = 
                  r[0 + 2 * dofs[1].size() * rot_coefs.rotation_n_nodes] * c1 + phi(2) * c2;
            }
            Eigen::Vector3d n_phi = phi / normphi;
            Eigen::Matrix3d K = (res * n_phi.transpose() + n_phi * res.transpose() + 
                                    n_phi.dot(res) * (Eigen::Matrix3d().setIdentity() - n_phi * n_phi.transpose() * 3.)
                                ) * (pi2 / normphi2);
            double * r = ufca.A.data() + (rot_args[1].start_rotation_index + n) * dofs[1].size() + rot_args[1].start_rotation_index + n;
            for (std::size_t ir = 0; ir < 3; ir++)
              for (std::size_t ic = 0; ic < 3; ic++)
                r[0 + dofs[1].size() * (ir * rot_coefs.rotation_n_nodes) + ic * rot_coefs.rotation_n_nodes] -= K(ir, ic);
                                
            // Rank 1 on the left (test function)
            const double c2 = res.dot(phi) * pi2 / normphi2 / normphi;
            rb[0] = res(0) * c1 + phi(0) * c2;
            rb[0 + rot_coefs.rotation_n_nodes] = res(1) * c1 + phi(1) * c2;
            rb[0 + 2 * rot_coefs.rotation_n_nodes] = res(2) * c1 + phi(2) * c2;
          }
        }
      }
    }
    
    
    // Add entries to global tensor. 
    A.add_local(ufca.A.data(), dofs);
    L.add_local(ufcb.A.data(), dofs);

    p++;
  }
}
//-----------------------------------------------------------------------------
void RotationAssemblerSystem::assemble_exterior_facets(
  GenericMatrix& A,
  const Form& a,
  GenericVector& L,
  const Form& b,
  UFC& ufca,
  UFC& ufcb,
  std::shared_ptr<const MeshFunction<std::size_t>> domains,
  std::vector<double>* values)
{
  // Skip assembly if there are no exterior facet integrals
  if (!(ufca.form.has_exterior_facet_integrals()||ufcb.form.has_exterior_facet_integrals()))
    return;

  // Set timer
  Timer timer("Assemble exterior facets");

  // Extract mesh
  dolfin_assert(a.mesh());
  const Mesh& mesh = *(a.mesh());

  // Form rank
  const std::size_t form_rank = ufca.form.rank();

  // Collect pointers to dof maps
  std::vector<const GenericDofMap*> dofmaps;
  for (std::size_t i = 0; i < form_rank; ++i)
    dofmaps.push_back(a.function_space(i)->dofmap().get());

  // Vector to hold dof map for a cell
  std::vector<ArrayView<const dolfin::la_index>> dofs(form_rank);

  // Exterior facet integral
  const ufc::exterior_facet_integral* integrala
    = ufca.default_exterior_facet_integral.get();
  const ufc::exterior_facet_integral* integralb
    = ufcb.default_exterior_facet_integral.get();

  // Check whether integral is domain-dependent
  bool use_domains = domains && !domains->empty();

  // Compute facets and facet - cell connectivity if not already computed
  const std::size_t D = mesh.topology().dim();
  mesh.init(D - 1);
  mesh.init(D - 1, D);
  dolfin_assert(mesh.ordered());

  // Assemble over exterior facets (the cells of the boundary)
  ufc::cell ufc_cell;
  std::vector<double> coordinate_dofs;
  Progress p(AssemblerBase::progress_message(A.rank(), "exterior facets"),
             mesh.num_facets());
  for (FacetIterator facet(mesh); !facet.end(); ++facet)
  {
    // Only consider exterior facets
    if (!facet->exterior())
    {
      p++;
      continue;
    }

    // Get integral for sub domain (if any)
    if (use_domains)
    {
      integrala = ufca.get_exterior_facet_integral((*domains)[*facet]);
      integralb = ufcb.get_exterior_facet_integral((*domains)[*facet]);
    }

    // Skip integral if zero
    if (!(integrala || integralb))
      continue;

    // Get mesh cell to which mesh facet belongs (pick first, there is
    // only one)
    dolfin_assert(facet->num_entities(D) == 1);
    Cell mesh_cell(mesh, facet->entities(D)[0]);

    // Check that cell is not a ghost
    dolfin_assert(!mesh_cell.is_ghost());

    // Get local index of facet with respect to the cell
    const std::size_t local_facet = mesh_cell.index(*facet);

    // Update UFC cell
    mesh_cell.get_cell_data(ufc_cell, local_facet);
    mesh_cell.get_coordinate_dofs(coordinate_dofs);

    // Update UFC object
    ufca.update(mesh_cell, coordinate_dofs, ufc_cell,
               integrala->enabled_coefficients());
    ufcb.update(mesh_cell, coordinate_dofs, ufc_cell,
               integralb->enabled_coefficients());

    // Get local-to-global dof maps for cell
    for (std::size_t i = 0; i < form_rank; ++i) {
      auto dmap = dofmaps[i]->cell_dofs(mesh_cell.index());
      dofs[i] = ArrayView<const dolfin::la_index>(dmap.size(), dmap.data());
    }

    // Tabulate exterior facet tensor
    integrala->tabulate_tensor(ufca.A.data(),
                              ufca.w(),
                              coordinate_dofs.data(),
                              local_facet,
                              ufc_cell.orientation);
    integralb->tabulate_tensor(ufcb.A.data(),
                              ufcb.w(),
                              coordinate_dofs.data(),
                              local_facet,
                              ufc_cell.orientation);

    // Add entries to global tensor
    A.add_local(ufca.A.data(), dofs);
    L.add_local(ufcb.A.data(), dofs);

    p++;
  }
}
//-----------------------------------------------------------------------------
void RotationAssemblerSystem::assemble_interior_facets(
  GenericMatrix& A,
  const Form& a,
  GenericVector& L,
  const Form& b,
  UFC& ufca,
  UFC& ufcb,
  std::shared_ptr<const MeshFunction<std::size_t>> domains,
  std::shared_ptr<const MeshFunction<std::size_t>> cell_domains,
  std::vector<double>* values)
{
  // Skip assembly if there are no interior facet integrals
  if (!(ufca.form.has_interior_facet_integrals()||ufcb.form.has_interior_facet_integrals()))
    return;

  dolfin_not_implemented();

  // Set timer
  Timer timer("Assemble interior facets");

  // Extract mesh and coefficients
  dolfin_assert(a.mesh());
  const Mesh& mesh = *(a.mesh());

  // MPI rank
  const int my_mpi_rank = MPI::rank(mesh.mpi_comm());

  // Form rank
  const std::size_t form_rank = ufca.form.rank();

  // Collect pointers to dof maps
  std::vector<const GenericDofMap*> dofmaps;
  for (std::size_t i = 0; i < form_rank; ++i)
    dofmaps.push_back(a.function_space(i)->dofmap().get());

  // Vector to hold dofs for cells, and a vector holding pointers to same
  std::vector<std::vector<dolfin::la_index>> macro_dofs(form_rank);
  std::vector<ArrayView<const dolfin::la_index>> macro_dof_ptrs(form_rank);

  // Interior facet integral
  const ufc::interior_facet_integral* integrala
    = ufca.default_interior_facet_integral.get();
  const ufc::interior_facet_integral* integralb
    = ufcb.default_interior_facet_integral.get();

  // Check whether integral is domain-dependent
  bool use_domains = domains && !domains->empty();
  bool use_cell_domains = cell_domains && !cell_domains->empty();

  // Compute facets and facet - cell connectivity if not already computed
  const std::size_t D = mesh.topology().dim();
  mesh.init(D - 1);
  mesh.init(D - 1, D);
  dolfin_assert(mesh.ordered());

  // Assemble over interior facets (the facets of the mesh)
  ufc::cell ufc_cell[2];
  std::vector<double> coordinate_dofs[2];
  Progress p(AssemblerBase::progress_message(A.rank(), "interior facets"),
             mesh.num_facets());
  for (FacetIterator facet(mesh); !facet.end(); ++facet)
  {
    if (facet->num_entities(D) == 1)
      continue;

    // Check that facet is not a ghost
    dolfin_assert(!facet->is_ghost());

    // Get integral for sub domain (if any)
    if (use_domains) {
      integrala = ufca.get_interior_facet_integral((*domains)[*facet]);
      integralb = ufcb.get_interior_facet_integral((*domains)[*facet]);
    }

    // Skip integral if zero
    if (!(integrala || integralb))
      continue;

    // Get cells incident with facet (which is 0 and 1 here is arbitrary)
    dolfin_assert(facet->num_entities(D) == 2);
    std::size_t cell_index_plus = facet->entities(D)[0];
    std::size_t cell_index_minus = facet->entities(D)[1];

    if (use_cell_domains && (*cell_domains)[cell_index_plus]
        < (*cell_domains)[cell_index_minus])
    {
      std::swap(cell_index_plus, cell_index_minus);
    }

    // The convention '+' = 0, '-' = 1 is from ffc
    const Cell cell0(mesh, cell_index_plus);
    const Cell cell1(mesh, cell_index_minus);

    // Get local index of facet with respect to each cell
    std::size_t local_facet0 = cell0.index(*facet);
    std::size_t local_facet1 = cell1.index(*facet);

    // Update to current pair of cells
    cell0.get_cell_data(ufc_cell[0], local_facet0);
    cell0.get_coordinate_dofs(coordinate_dofs[0]);
    cell1.get_cell_data(ufc_cell[1], local_facet1);
    cell1.get_coordinate_dofs(coordinate_dofs[1]);

    ufca.update(cell0, coordinate_dofs[0], ufc_cell[0],
               cell1, coordinate_dofs[1], ufc_cell[1],
               integrala->enabled_coefficients());
    ufcb.update(cell0, coordinate_dofs[0], ufc_cell[0],
               cell1, coordinate_dofs[1], ufc_cell[1],
               integralb->enabled_coefficients());

    // Tabulate dofs for each dimension on macro element
    for (std::size_t i = 0; i < form_rank; i++)
    {
      // Get dofs for each cell
      auto cell_dofs0 = dofmaps[i]->cell_dofs(cell0.index());
      auto cell_dofs1 = dofmaps[i]->cell_dofs(cell1.index());

      // Create space in macro dof vector
      macro_dofs[i].resize(cell_dofs0.size() + cell_dofs1.size());

      // Copy cell dofs into macro dof vector
      std::copy(cell_dofs0.data(), cell_dofs0.data() + cell_dofs0.size(),
                macro_dofs[i].begin());
      std::copy(cell_dofs1.data(), cell_dofs1.data() + cell_dofs1.size(),
                macro_dofs[i].begin() + cell_dofs0.size());
      macro_dof_ptrs[i].set(macro_dofs[i]);
    }

    // Tabulate interior facet tensor on macro element
    integrala->tabulate_tensor(ufca.macro_A.data(),
                              ufca.macro_w(),
                              coordinate_dofs[0].data(),
                              coordinate_dofs[1].data(),
                              local_facet0,
                              local_facet1,
                              ufc_cell[0].orientation,
                              ufc_cell[1].orientation);
    integralb->tabulate_tensor(ufcb.macro_A.data(),
                              ufcb.macro_w(),
                              coordinate_dofs[0].data(),
                              coordinate_dofs[1].data(),
                              local_facet0,
                              local_facet1,
                              ufc_cell[0].orientation,
                              ufc_cell[1].orientation);

    if (cell0.is_ghost() != cell1.is_ghost())
    {
      int ghost_rank = -1;
      if (cell0.is_ghost())
        ghost_rank = cell0.owner();
      else
        ghost_rank = cell1.owner();

      dolfin_assert(my_mpi_rank != ghost_rank);
      dolfin_assert(ghost_rank != -1);
      if (ghost_rank < my_mpi_rank)
        continue;
    }

    // Add entries to global tensor
    A.add_local(ufca.macro_A.data(), macro_dof_ptrs);
    L.add_local(ufcb.macro_A.data(), macro_dof_ptrs);

    p++;
  }
}
//-----------------------------------------------------------------------------
void RotationAssemblerSystem::assemble_vertices(
  GenericMatrix& A,
  const Form& a,
  GenericVector& L,
  const Form& b,
  UFC& ufca,
  UFC& ufcb,
  std::shared_ptr<const MeshFunction<std::size_t>> domains)
{
  // Skip assembly if there are no point integrals
  if (!(ufca.form.has_vertex_integrals() || ufca.form.has_vertex_integrals()))
    return;

  dolfin_not_implemented();

  // Set timer
  Timer timer("Assemble vertices");

  // Extract mesh
  dolfin_assert(a.mesh());
  const Mesh& mesh = *(a.mesh());

  // Compute cell and vertex - cell connectivity if not already
  // computed
  const std::size_t D = mesh.topology().dim();
  mesh.init(0);
  mesh.init(0, D);
  dolfin_assert(mesh.ordered());

  // Logics for shared vertices
  const bool has_shared_vertices = mesh.topology().have_shared_entities(0);
  const std::map<std::int32_t, std::set<unsigned int>>&
    shared_vertices = mesh.topology().shared_entities(0);

  // Form rank
  const std::size_t form_rank = ufca.form.rank();

  // Collect pointers to dof maps
  std::vector<const GenericDofMap*> dofmaps(form_rank);

  // Create a vector for storying local to local map for vertex entity
  // dofs
  std::vector<std::vector<std::size_t>> local_to_local_dofs(form_rank);

  // Create a values vector to be used to fan out local tabulated
  // values to the global tensor
  std::vector<double> local_values(1);

  // Vector to hold local dof map for a vertex
  std::vector<std::vector<dolfin::la_index>> global_dofs(form_rank);
  std::vector<ArrayView<const dolfin::la_index>> global_dofs_p(form_rank);
  std::vector<dolfin::la_index> local_dof_size(form_rank);
  for (std::size_t i = 0; i < form_rank; ++i)
  {
    dofmaps[i] = a.function_space(i)->dofmap().get();

    // Check that the test and trial space as dofs on the vertices
    if (dofmaps[i]->num_entity_dofs(0) == 0)
    {
      dolfin_error("Assembler.cpp",
                   "assemble form over vertices",
                   "Expecting test and trial spaces to have dofs on "\
                   "vertices for point integrals");
    }

    // Check that the test and trial spaces do not have dofs other
    // than on vertices
    for (std::size_t j = 1; j <= D; j++)
    {
      if (dofmaps[i]->num_entity_dofs(j)!=0)
      {
        dolfin_error("Assembler.cpp",
                     "assemble form over vertices",
                     "Expecting test and trial spaces to only have dofs on " \
                     "vertices for point integrals");
      }
    }

    // Resize local values so it can hold dofs on one vertex
    local_values.resize(local_values.size()*dofmaps[i]->num_entity_dofs(0));

    // Resize local to local map according to the number of vertex
    // entities dofs
    local_to_local_dofs[i].resize(dofmaps[i]->num_entity_dofs(0));

    // Resize local dof map vector
    global_dofs[i].resize(dofmaps[i]->num_entity_dofs(0));

    // Get size of local dofs
    local_dof_size[i] = dofmaps[i]->ownership_range().second
      - dofmaps[i]->ownership_range().first;

    // Get pointer to global dofs
    global_dofs_p[i].set(global_dofs[i]);
  }

  // Vector to hold dof map for a cell
  std::vector<ArrayView<const dolfin::la_index>> dofs(form_rank);

  // Exterior point integral
  const ufc::vertex_integral* integrala
    = ufca.default_vertex_integral.get();
  const ufc::vertex_integral* integralb
    = ufcb.default_vertex_integral.get();

  // Check whether integral is domain-dependent
  bool use_domains = domains && !domains->empty();

  // MPI rank
  const unsigned int my_mpi_rank = MPI::rank(mesh.mpi_comm());

  // Assemble over vertices
  ufc::cell ufc_cell;
  std::vector<double> coordinate_dofs;
  Progress p(AssemblerBase::progress_message(A.rank(), "vertices"),
             mesh.num_vertices());
  for (VertexIterator vert(mesh); !vert.end(); ++vert)
  {
    // Get integral for sub domain (if any)
    if (use_domains)
    {
      integrala = ufca.get_vertex_integral((*domains)[*vert]);
      integralb = ufcb.get_vertex_integral((*domains)[*vert]);
    }
    // Skip integral if zero
    if (!(integrala || integralb))
      continue;

    // Check if assembling a scalar and a vertex is shared
    if (form_rank == 0 && has_shared_vertices)
    {
      // Find shared processes for this global vertex
      std::map<std::int32_t, std::set<unsigned int>>::const_iterator e;
      e = shared_vertices.find(vert->index());

      // If vertex is shared and this rank is not the lowest do not
      // include the contribution from this vertex to scalar sum
      if (e != shared_vertices.end())
      {
        bool skip_vertex = false;
        std::set<unsigned int>::const_iterator it;
        for (it = e->second.begin(); it != e->second.end(); it++)
        {
          // Check if a shared vertex has a lower process rank
          if (*it < my_mpi_rank)
          {
            skip_vertex = true;
            break;
          }
        }

        if (skip_vertex)
          continue;
      }
    }

    // Get mesh cell to which mesh vertex belongs (pick first)
    Cell mesh_cell(mesh, vert->entities(D)[0]);

    // Check that cell is not a ghost
    dolfin_assert(!mesh_cell.is_ghost());

    // Get local index of vertex with respect to the cell
    const std::size_t local_vertex = mesh_cell.index(*vert);

    // Update UFC cell
    mesh_cell.get_cell_data(ufc_cell);
    mesh_cell.get_coordinate_dofs(coordinate_dofs);

    // Update UFC object
    ufca.update(mesh_cell, coordinate_dofs, ufc_cell,
               integrala->enabled_coefficients());
    ufcb.update(mesh_cell, coordinate_dofs, ufc_cell,
               integralb->enabled_coefficients());

    // Tabulate vertex tensor
    integrala->tabulate_tensor(ufca.A.data(),
                              ufca.w(),
                              coordinate_dofs.data(),
                              local_vertex,
                              ufc_cell.orientation);
    integralb->tabulate_tensor(ufcb.A.data(),
                              ufcb.w(),
                              coordinate_dofs.data(),
                              local_vertex,
                              ufc_cell.orientation);

    // For rank 1 and 2 tensors we need to check if tabulated dofs for
    // the test space is within the local range
    bool owns_all_dofs = true;
    for (std::size_t i = 0; i < form_rank; ++i)
    {
      // Get local-to-global dof maps for cell
      auto dmap = dofmaps[i]->cell_dofs(mesh_cell.index());
      dofs[i].set(dmap.size(), dmap.data());

      // Get local dofs of the local vertex
      dofmaps[i]->tabulate_entity_dofs(local_to_local_dofs[i], 0, local_vertex);

      // Copy cell dofs to local dofs and check owner ship range
      for (std::size_t j = 0; j < local_to_local_dofs[i].size(); ++j)
      {
        global_dofs[i][j] = dofs[i][local_to_local_dofs[i][j]];

        // It is the dofs for the test space that determines if a dof
        // is owned by a process, therefore i==0
        if (i == 0 && global_dofs[i][j] >= local_dof_size[i])
        {
          owns_all_dofs = false;
          break;
        }
      }
    }

    // If not owning all dofs
    if (!owns_all_dofs)
      continue;

    // Form rank 1
    {
      // Copy tabulated tensor to local value vector
      for (std::size_t i = 0; i < local_to_local_dofs[0].size(); ++i)
        local_values[i] = ufcb.A[local_to_local_dofs[0][i]];

      // Add local entries to global tensor
      L.add_local(local_values.data(), global_dofs_p);
    }
    //Form rank 2
    {
      // Copy tabulated tensor to local value vector
      const std::size_t num_cols = dofs[1].size();
      for (std::size_t i = 0; i < local_to_local_dofs[0].size(); ++i)
      {
        for (std::size_t j = 0; j < local_to_local_dofs[1].size(); ++j)
        {
          local_values[i*local_to_local_dofs[1].size() + j]
            = ufca.A[local_to_local_dofs[0][i]*num_cols
                    + local_to_local_dofs[1][j]];
        }
      }

      // Add local entries to global tensor
      A.add_local(local_values.data(), global_dofs_p);
    }

    p++;
  }
}
//-----------------------------------------------------------------------------
}

PYBIND11_MODULE(SIGNATURE, m)
{
    pybind11::class_<dolfin::RotationAssemblerSystem, std::shared_ptr<dolfin::RotationAssemblerSystem>, dolfin::AssemblerBase>
      (m, "RotationAssemblerSystem", "DOLFIN RotationAssemblerSystem object")
    .def(pybind11::init<>())
    .def("assemble", &dolfin::RotationAssemblerSystem::assemble);
}

