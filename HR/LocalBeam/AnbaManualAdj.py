#
# Copyright (C) 2018 Marco Morandini
#

import dolfin as do
import ufl
#import time

do.set_log_level(do.LogLevel.CRITICAL)

do.parameters["form_compiler"]["optimize"] = True
do.parameters["form_compiler"]["cpp_optimize"] = True
#parameters["reorder_dofs_serial"] = False
do.parameters["form_compiler"]["quadrature_degree"] = 1
#parameters["form_compiler"]["no-evaluate_basis_derivatives"] = False

#set_log_level(LogLevel.PROGRESS)

# ufl_action = action
# def action(A, x):
#     A = ufl.algorithms.expand_derivatives(A)
#     if len(A.integrals()) != 0: # form is not empty:
#         return ufl_action(A, x)
#     else:
#         return A # form is empty, doesn't matter anyway

class BeamMaterial:
    def __init__(self, E, nu, psi):
            self.E = do.Constant(E)
            self.nu = do.Constant(nu)
            #self.mu = E / (2.0*(1.0 + nu))
            #self.lmbda = E*nu / ((1.0 + nu)*(1.0 - 2.0*nu))
            self.psi = psi(self.E, self.nu)

    def firstpksigma(self, F):
        "Return first PK stress tensor."
        #F = defgrad
        F = do.variable(F)
        first_PK = do.diff(self.psi(F), F)
        return first_PK

    def secondpksigma(self, F):
        "Return second PK stress tensor."
        first_PK = self.firstpksigma(F)
        return do.dot(ufl.inv(F), first_PK)

def epsilon(defgrad):
    "Return symmetric 3D deformation tensor."
    return 0.5*(do.dot(defgrad.T, defgrad) - do.Identity(3)) #

def GreenElasticPsi(E, nu):
    "Return Green elastic internal energy."
    mu = E / (2.0*(1.0 + nu))
    lmbda = E*nu / ((1.0 + nu)*(1.0 - 2.0*nu))
    def energy(defgrad):
        eps = epsilon(defgrad)
        w = mu * do.inner(eps, eps) + 0.5 * lmbda * do.tr(eps) * do.tr(eps)
        return w

    return energy

def NeoHookeanPsi(E, nu):
    "Return NeoHookean (Abaqus version) internal energy."
    mu = E / (2.0*(1.0 + nu))
    C10 = mu / 2.
    K0 = E / (3. * (1. - 2. * nu))
    def energy(defgrad):
        #F = defgrad
        J = do.det(defgrad)
        #Fdev = pow(J, -1./3.) * F
        #I1 = tr(Fdev.T*Fdev)
        I1 = pow(J, -2./3.) * do.tr(defgrad.T * defgrad)
        psi = C10 * (I1 - 3.) + (J - 1.) * (J - 1.) * K0 / 2.
        return psi

    return energy

class BeamSection():
    def __init__(self, idx, material):
        self.idx = idx
        self.FORCE_IA = do.Constant((0., 0., 0.))
        self.MOMENT_IA = do.Constant((0., 0., 0.))
        self.material = material

        self.mesh = do.Mesh(do.UnitSquareMesh(10, 10))
        do.ALE.move(self.mesh, do.Constant([-0.5, -0.5]))

        Z_ELEMENT = do.FiniteElement("DG", self.mesh.ufl_cell(), 0)
        Z_SPACE = do.FunctionSpace(self.mesh, Z_ELEMENT)
        Z = do.Function(Z_SPACE)

        Z.interpolate(do.Constant(0.))

        element_order = 1

        UF3_ELEMENT = do.VectorElement("CG", self.mesh.ufl_cell(), element_order, 3)


        #Lagrange multipliers needed to compute the stress resultants and moment resultants
        R3_ELEMENT = do.VectorElement("R", self.mesh.ufl_cell(), 0, 3)
        RLAGR_ELEMENT = do.VectorElement("R", self.mesh.ufl_cell(), 0, 6)

        FU3_6_ELEMENT = do.MixedElement(UF3_ELEMENT, UF3_ELEMENT, R3_ELEMENT, R3_ELEMENT, RLAGR_ELEMENT)
        FU3_6_SPACE = do.FunctionSpace(self.mesh, FU3_6_ELEMENT)
        self.FU3_6 = do.Function(FU3_6_SPACE, name='u')
        FU3_6_splitted = do.split(self.FU3_6)
        VU3_6 = do.TestFunction(FU3_6_SPACE)
        TU3_6 = do.TrialFunction(FU3_6_SPACE)
        VU3_6_splitted = do.split(VU3_6)
        TU3_6_splitted = do.split(TU3_6)

        ADJ_ELEMENT = do.MixedElement(R3_ELEMENT, R3_ELEMENT)
        ADJ_FM_SPACE = do.FunctionSpace(self.mesh, ADJ_ELEMENT)
        V_ADJ_FM = do.TestFunction(ADJ_FM_SPACE)
        V_ADJ_FM_splitted = do.split(V_ADJ_FM)
        T_ADJ_FM = do.TrialFunction(ADJ_FM_SPACE)
        T_ADJ_FM_splitted = do.split(T_ADJ_FM)

        ADJ_SPACE = do.FunctionSpace(self.mesh, R3_ELEMENT)
        self.ADJ_F = do.Function(ADJ_SPACE)
        self.ADJ_M = do.Function(ADJ_SPACE)

        self.f_dot = do.Function(ADJ_FM_SPACE)


        POS = do.SpatialCoordinate(self.mesh)
        POSp = self.pos3d(POS) + FU3_6_splitted[0]

        Torsion = do.as_tensor([-POS[1], POS[0], 0.])
        Flex_y = do.as_tensor([0., 0., -POS[0]])
        Flex_x = do.as_tensor([0., 0., POS[1]])

        X_translation = do.as_tensor([1., 0., 0.]) #Constant((1., 0., 0.))
        Y_translation = do.as_tensor([0., 1., 0.]) #Constant((0., 1., 0.))
        Z_translation = do.as_tensor([0., 0., 1.]) #Constant((0., 0., 1.))

        # AZIONI INTERNE
        #forze x : FU3_6.sub(6)[0] (taglio x)
        #forze y : FU3_6.sub(6)[1] (taglio y)
        #forze z : FU3_6.sub(6)[2] (az assiale z)

        # MOMENTI
        # Mx     : FU3_6.sub(7)[0] (flettente x)
        # My     : FU3_6.sub(7)[1] (flettente y)
        # Mz     : FU3_6.sub(7)[2] (torsione z)

        U = FU3_6_splitted[0]
        U += FU3_6_splitted[1] * Z

        dUdz = do.diff(U, Z)

        F = self.DefGradient(U, dUdz)
        eps = epsilon(F)

        delta_F = do.derivative(F, self.FU3_6, VU3_6)

        #psi = NeoHookeanPsi
        S = self.material.firstpksigma(F)
        #S = firstpksigma(F, NeoHookeanPsi)

        #delta_S = derivative(S, FU3_6, VU3_6)
        T = self.material.secondpksigma(F)
        dT_deltau = do.derivative(T, self.FU3_6, VU3_6)

        self.Sn = S[:, 2]
        self.S1 = S[:, 0]
        self.S2 = S[:, 1]
        Ss = do.as_tensor([[self.S1[0], self.S2[0]],
                           [self.S1[1], self.S2[1]],
                           [self.S1[2], self.S2[2]]])

        delta_Fn = delta_F[:, 2]
        delta_F1 = delta_F[:, 0]
        delta_F2 = delta_F[:, 1]
        delta_Fs = do.as_tensor([[delta_F1[0], delta_F2[0]],
                                 [delta_F1[1], delta_F2[1]],
                                 [delta_F1[2], delta_F2[2]]])

        #dS_dz = diff(S, Z)
        dSn_dz = do.diff(self.Sn, Z)
        dSn_dz2 = do.diff(dSn_dz, Z)
        dSs_dz = do.diff(Ss, Z)

        delta_Fn_dz = do.diff(delta_Fn, Z)
        ddelta_Fs_dz = do.diff(delta_Fs, Z)

        self.lambda_adj = do.Function(FU3_6_SPACE)
        self.dv_df = do.Function(ADJ_FM_SPACE)
        self.dFm_dmdot = do.Function(FU3_6_SPACE)

        self.U_tlm = [do.Function(FU3_6_SPACE), do.Function(FU3_6_SPACE), do.Function(FU3_6_SPACE),
                      do.Function(FU3_6_SPACE), do.Function(FU3_6_SPACE), do.Function(FU3_6_SPACE)]
        self.U_soa = [do.Function(FU3_6_SPACE), do.Function(FU3_6_SPACE), do.Function(FU3_6_SPACE),
                      do.Function(FU3_6_SPACE), do.Function(FU3_6_SPACE), do.Function(FU3_6_SPACE)]

        self.J_ff = [do.Function(ADJ_FM_SPACE), do.Function(ADJ_FM_SPACE), do.Function(ADJ_FM_SPACE),
                     do.Function(ADJ_FM_SPACE), do.Function(ADJ_FM_SPACE), do.Function(ADJ_FM_SPACE)]

        self.U_t = do.Function(FU3_6_SPACE)

        self.element_tn_project = do.VectorElement("CG", self.mesh.ufl_cell(), 1, 3)
        self.V_tn_project = do.FunctionSpace(self.mesh, self.element_tn_project)
        self.t1_project = do.Function(self.V_tn_project, name='t1')
        self.t2_project = do.Function(self.V_tn_project, name='t2')
        self.tn_project = do.Function(self.V_tn_project, name='tn')
        self.t1_project.vector()[:] = do.project(self.S1, self.V_tn_project, solver_type='gmres').vector()
        self.t2_project.vector()[:] = do.project(self.S2, self.V_tn_project, solver_type='gmres').vector()

        self.filex = do.XDMFFile('Displ_BeamSection_'+str(self.idx)+'.xdmf')
        self.filex.parameters['functions_share_mesh'] = True
        self.filex.parameters['rewrite_function_mesh'] = False
        self.filex.parameters["flush_output"] = True

        self.filex.write(self.FU3_6.sub(0), 0.)
        self.filex.write(self.FU3_6.sub(1), 0.)
        self.filex.write(self.t1_project, t=0.)
        self.filex.write(self.t2_project, t=0.)
        self.filex.write(self.tn_project, t=0.)

        Eq = do.inner(delta_Fs, Ss) * do.dx
        Eq += -do.dot(VU3_6_splitted[0], dSn_dz) * do.dx
        Eq += -do.dot(VU3_6_splitted[0], dSn_dz2) * do.dx - \
              do.dot(VU3_6_splitted[1], dSn_dz) * do.dx
        Eq += do.inner(ddelta_Fs_dz, Ss) * do.dx + \
              do.inner(delta_Fs, dSs_dz) * do.dx

        Force = do.dot(VU3_6_splitted[2], self.Sn) * do.dx - \
                do.dot(VU3_6_splitted[2], self.FORCE_IA) * do.dx
        Force += do.dot(FU3_6_splitted[2], do.derivative(self.Sn, self.FU3_6, VU3_6)) * do.dx
        Moment = do.dot(VU3_6_splitted[3], do.cross(POSp, self.Sn)) * do.dx - \
                 do.dot(VU3_6_splitted[3], self.MOMENT_IA) * do.dx
        Moment += do.dot(FU3_6_splitted[3], do.derivative(do.cross(POSp, self.Sn), self.FU3_6, VU3_6)) * do.dx

        self.LinearForm = Eq + Force + Moment
        self.LinearForm += VU3_6_splitted[4][0] * FU3_6_splitted[0][0] * do.dx
        self.LinearForm += FU3_6_splitted[4][0] * VU3_6_splitted[0][0] * do.dx
        self.LinearForm += VU3_6_splitted[4][1] * FU3_6_splitted[0][1] * do.dx
        self.LinearForm += FU3_6_splitted[4][1] * VU3_6_splitted[0][1] * do.dx
        self.LinearForm += VU3_6_splitted[4][2] * FU3_6_splitted[0][2] * do.dx
        self.LinearForm += FU3_6_splitted[4][2] * VU3_6_splitted[0][2] * do.dx
        self.LinearForm += VU3_6_splitted[4][3] * do.dot(FU3_6_splitted[0], Flex_x) * do.dx
        self.LinearForm += FU3_6_splitted[4][3] * do.dot(VU3_6_splitted[0], Flex_x) * do.dx
        self.LinearForm += VU3_6_splitted[4][4] * do.dot(FU3_6_splitted[0], Flex_y) * do.dx
        self.LinearForm += FU3_6_splitted[4][4] * do.dot(VU3_6_splitted[0], Flex_y) * do.dx
        self.LinearForm += VU3_6_splitted[4][5] * do.dot(FU3_6_splitted[0], Torsion) * do.dx
        self.LinearForm += FU3_6_splitted[4][5] * do.dot(VU3_6_splitted[0], Torsion) * do.dx

        self.dLinearForm_du = do.derivative(self.LinearForm, self.FU3_6, TU3_6)
        #adj_dLinearForm_du = adjoint(dLinearForm_du,
        #                             reordered_arguments=ufl.algorithms.extract_arguments(dLinearForm_du))
        self.adj_dLinearForm_du = do.adjoint(self.dLinearForm_du)
        #current_args = ufl.algorithms.extract_arguments(adj_dLinearForm_du)
        #correct_args = [VU3_6, TU3_6]
        #adj_dLinearForm_du = ufl.replace(adj_dLinearForm_du, dict(zip(current_args, correct_args)))

        minusdLinearForm_df = do.dot(T_ADJ_FM_splitted[0], VU3_6_splitted[2]) * do.dx + \
                              do.dot(T_ADJ_FM_splitted[1], VU3_6_splitted[3]) * do.dx
        self.minusdLinearForm_df_ass = do.assemble(minusdLinearForm_df)
        minus_adj_dLinearForm_df = do.dot(V_ADJ_FM_splitted[0], TU3_6_splitted[2]) * do.dx + \
                                   do.dot(V_ADJ_FM_splitted[1], TU3_6_splitted[3]) * do.dx
        self.minus_adj_dLinearForm_df_ass = do.assemble(minus_adj_dLinearForm_df)

        self.dv_du = do.inner(dT_deltau, eps) * do.dx
        self.v_compl = (do.inner(T, eps) - self.material.psi(F)) * do.dx
        
        self.dFdudu = do.derivative(self.adj_dLinearForm_du, self.FU3_6, self.U_t)
        self.rhs_form = -do.action(self.dFdudu, self.lambda_adj) + do.derivative(self.dv_du, self.FU3_6, self.U_t)

    def grad3d(self, u, up):
        "Return 3d gradient."
        g = do.grad(u)
        return do.as_tensor([[g[0, 0], g[0, 1], up[0]],
                             [g[1, 0], g[1, 1], up[1]],
                             [g[2, 0], g[2, 1], up[2]]])

    def DefGradient(self, u, up):
        "Return 3d deformation gradient."
        #return grad3d(u, up) + Identity(3)
        g = do.grad(u)
        return do.as_tensor([[g[0, 0] + 1., g[0, 1], up[0]],
                             [g[1, 0], g[1, 1] + 1., up[1]],
                             [g[2, 0], g[2, 1], up[2] + 1.]])

    def pos3d(self, POS):
        return do.as_vector([POS[0], POS[1], 0.])

    def tlm(self, dLinearForm_du_ass, f_dot, u_tlm):
        # dFmdm = expand(derivative(Fm, m, m_dot))
        self.minusdLinearForm_df_ass.mult(f_dot.vector(), self.dFm_dmdot.vector())
        do.solve(dLinearForm_du_ass, u_tlm.vector(), self.dFm_dmdot.vector())
        return u_tlm

    def all_tlm(self):
        dLinearForm_du_ass = do.assemble(self.dLinearForm_du)
        for i in range(6):
            self.f_dot.vector().zero()
            if self.f_dot.vector().owns_index(i):
                j = i - self.f_dot.vector().local_range()[0]
                self.f_dot.vector()[j] = 1.
            self.tlm(dLinearForm_du_ass, self.f_dot, self.U_tlm[i])

    def soa(self, i):
        self.U_t.vector()[:] = self.U_tlm[i].vector()
        rhside = do.assemble(self.rhs_form)
        do.solve(self.adj_dLinearForm_du_ass, self.U_soa[i].vector(), rhside)

    def all_soa(self):
        for i in range(6):
            self.soa(i)
    
    def solve(self, force, moment):
        self.FORCE_IA.assign(force)
        self.MOMENT_IA.assign(moment)
        do.solve(self.LinearForm == 0, self.FU3_6)
        
    def output(self, t):
        self.filex.write(self.FU3_6.sub(0), t=t)
        self.filex.write(self.FU3_6.sub(1), t=t)
        self.t1_project.vector()[:] = do.project(self.S1, self.V_tn_project,
                                                 solver_type='gmres').vector()
        self.t2_project.vector()[:] = do.project(self.S2, self.V_tn_project,
                                                 solver_type='gmres').vector()
        self.tn_project.vector()[:] = do.project(self.Sn, self.V_tn_project,
                                                 solver_type='gmres').vector()
        self.filex.write(self.t1_project, t=t)
        self.filex.write(self.t2_project, t=t)
        self.filex.write(u=self.tn_project, t=t)

    def delta_v(self, force, moment):
        self.solve(force, moment)
        
        dv_du_ass = do.assemble(self.dv_du)
        self.adj_dLinearForm_du_ass = do.assemble(self.adj_dLinearForm_du)
        do.solve(self.adj_dLinearForm_du_ass, self.lambda_adj.vector(), dv_du_ass)
        self.minus_adj_dLinearForm_df_ass.mult(self.lambda_adj.vector(), self.dv_df.vector())
        return self.dv_df

    def de_delta_v(self, force, moment):
        self.delta_v(force, moment)
        self.all_tlm()
        self.all_soa()
        for icomp in range(6):
            self.minus_adj_dLinearForm_df_ass.mult(self.U_soa[icomp].vector(), self.J_ff[icomp].vector())
            #print(self.J_ff[icomp].vector()[:])
        return (self.dv_df, self.J_ff)

#pause_annotation()
#continue_annotation()
if __name__ == "__main__":

    do.set_log_level(do.LogLevel.INFO)

    material = BeamMaterial(1, 0.33, GreenElasticPsi)
    #material = BeamMaterial(1, 0.33, NeoHookeanPsi)

    section = BeamSection(1, material)

    num_incs = 20
    FINAL_Tx = 0.
    FINAL_Ty = 0.05
    FINAL_N = 0.#0.05
    FINAL_Mx = 0.04 #0.005 #0.0
    FINAL_My = 0.0
    FINAL_Mz = 0. #0.1 #0.03
    for i in range(1, num_incs + 1, 1):
    #for i in range(1, 2): #range(1, num_incs + 1, 1):
        FORCE_IA = do.Constant((FINAL_Tx / num_incs * i,
                                FINAL_Ty / num_incs * i,
                                FINAL_N / num_incs * i))
        MOMENT_IA = do.Constant((FINAL_Mx / num_incs * i,
                                 FINAL_My / num_incs * i,
                                 FINAL_Mz / num_incs * i))
        section.solve(FORCE_IA, MOMENT_IA)
        section.output(i)
        section.de_delta_v(FORCE_IA, MOMENT_IA)

        #compl_en1 = do.assemble(v_compl)

