# 
# .. _Timoshenko_beam_implementation:
#
# Nonlinear Timoshenko beam implementation
# HR formulation
#
# ========================================

# First of all, import os ::

import os
import sys
#import copy
import numpy

fileout = sys.stdout

# followed by dolfin ::

from dolfin import *
import LocalBeam.AnbaManualAdj as Anba

# Set the ``form compiler`` parameters. 
# The ``representation`` ``uflacs`` is needed because
# it correctly handles the derivation of conditional UFL expressions ::
quad_scheme = "default"
quad_degree = 2

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

# Limit the quadrature order ::

#parameters["form_compiler"]["quadrature_rule"] = quad_scheme
parameters["form_compiler"]["quadrature_degree"] = quad_degree

# Import the rotation handling library ::

import RotVector

# and the customized assembly routines ::

from rotation_assembling import *

# Build the mesh with 200 elements along a straight line of lenght 10 ::

beam_len = 10.
#mesh = IntervalMesh(200, 0., beam_len)
mesh = IntervalMesh(2, 0., beam_len)
dx = Measure("dx")
dx = dx(degree=quad_degree, scheme=quad_scheme)

beam_sections = []
material = Anba.BeamMaterial(1, 0.33, Anba.GreenElasticPsi)
beam_sections = mesh.num_cells() * [None]
for i in range(mesh.num_cells()):
    beam_sections[i] = Anba.BeamSection(i, material)
    print('Just built beam section #',i)

class delta_v_expression(UserExpression):
    def __init__(self, u):
        UserExpression.__init__(self)
        self.u = u
        #self.prev_cell = None
        self.uvalues = numpy.array(u.value_shape()[0] * [0.], numpy.float64)
        self.Tc = Constant((0., 0., 0.))
        self.Mc = Constant((0., 0., 0.))
        self.previous_cell_idx = None
        self.previous_Tc = numpy.array(3 * [0.], numpy.float64)
        self.previous_Mc = numpy.array(3 * [0.], numpy.float64)
        self.cached_values = numpy.array(6 * [0.], numpy.float64)

    def eval_cell(self, value, x, ufc_cell):
        self.u.eval_cell(self.uvalues, x, ufc_cell)
        #print(self.uvalues)
        self.Tc.assign(Constant((self.uvalues[6], self.uvalues[7], self.uvalues[8])))
        self.Mc.assign(Constant((self.uvalues[9], self.uvalues[10], self.uvalues[11])))
        if (not(ufc_cell.index == self.previous_cell_idx and 
            numpy.array_equal(self.Tc.values(), self.previous_Tc) and 
            numpy.array_equal(self.Mc.values(), self.previous_Mc))):
            value[:] = beam_sections[ufc_cell.index].delta_v(self.Tc, self.Mc).vector()
            self.previous_cell_idx = ufc_cell.index
            self.previous_Tc[:] = self.Tc.values()
            self.previous_Mc[:] = self.Mc.values()
            self.cached_values[:] = value
            fileout.write(':')
        else:
            value[:] = self.cached_values
            fileout.write('.')
        fileout.flush()

    def value_shape(self):
        return (6,)

class de_delta_v_expression(UserExpression):
    def __init__(self, u):
        UserExpression.__init__(self)
        self.u = u
        #self.prev_cell = None
        self.uvalues = numpy.array(u.value_shape()[0] * [0.], numpy.float64)
        self.Tc = Constant((0., 0., 0.))
        self.Mc = Constant((0., 0., 0.))
        self.previous_cell_idx = None
        self.previous_Tc = numpy.array(3 * [0.], numpy.float64)
        self.previous_Mc = numpy.array(3 * [0.], numpy.float64)
        self.cached_values = numpy.array(36 * [0.], numpy.float64)

    def eval_cell(self, value, x, ufc_cell):
        self.u.eval_cell(self.uvalues, x, ufc_cell)
        self.Tc.assign(Constant((self.uvalues[6], self.uvalues[7], self.uvalues[8])))
        self.Mc.assign(Constant((self.uvalues[9], self.uvalues[10], self.uvalues[11])))
        if (not(ufc_cell.index == self.previous_cell_idx and 
            numpy.array_equal(self.Tc.values(), self.previous_Tc) and 
            numpy.array_equal(self.Mc.values(), self.previous_Mc))):
            (delta_v, de_delta_v) = beam_sections[ufc_cell.index].de_delta_v(self.Tc, self.Mc)
            for i in range(6):
                value[i * 6:(i + 1)*6] = de_delta_v[i].vector()
            self.previous_cell_idx = ufc_cell.index
            self.previous_Tc[:] = self.Tc.values()
            self.previous_Mc[:] = self.Mc.values()
            self.cached_values[:] = value
            fileout.write('X')
        else:
            value[:] = self.cached_values
            fileout.write('x')
        fileout.flush()

    def value_shape(self):
        return (6, 6)

    def value_rank(self):
        return 2

# Define two Lagrange (``CG``) vector element of ``size`` equal to 3. 
# The first one has a ``degree`` equal to 2, and will be used to approximate 
# the beam displacement. The second one has a ``degree`` equal to 1, 
# and will be used to approximate rotations. This choice greatly alleviates, if
# not eliminates completely, the problem of parasitic shear locking, 
# at the expense of an higher number on unknowns. ::

U_ELEMENT = VectorElement("CG", mesh.ufl_cell(), 1, 3)
PHI_ELEMENT = VectorElement("CG", mesh.ufl_cell(), 1, 3)
AZ_ELEMENT = VectorElement("DG", mesh.ufl_cell(), 0, 3)

# Define a ``Mixed element`` comprising the displacement and rotation elements ::

UNKS_ELEMENT = MixedElement(U_ELEMENT, PHI_ELEMENT, AZ_ELEMENT, AZ_ELEMENT)

# and the approximating function space of the problem ::

UNKS_SPACE = FunctionSpace(mesh, UNKS_ELEMENT)

# Define the Functions 
#
# * ``u`` to store the total displacement and rotation,
# * ``uinc`` to store the increment of the unknowns during the nonlinear iterative solution procedure
#
# ::

u = Function(UNKS_SPACE)
uinc = Function(UNKS_SPACE)

# Define also the ``TrialFunction`` and the ``TestFunction`` ::

du = TrialFunction(UNKS_SPACE)
v = TestFunction(UNKS_SPACE)

# Take a reference to the subspace basis function values using the UFL ``split`` method ::

u_u, u_phi, u_T, u_M = split(u)
du_u, du_phi, du_T, du_M = split(du)
v_u, v_phi, vu_T, vu_M = split(v)

# The helper class ``SwitchNodes`` is used to ease the computation
# of the complement rotation.
# 
# The class stores a reference to 
# the unknown function ``u``, and a local function ``utmp``, that should be 
# defined on the same space of u.
# It also stores a reference to the ``Expression`` ``RotVector.cppcode_unwrap_phi``,
# and assigns to its public member ``phi_function`` the function ``utmp``.
# In this way, whenever the ``eval`` method of ``RotVector.cppcode_unwrap_phi``,
# it will be applied to ``utmp``.
#
# When the ``Switch`` metod is called the values of ``u``
# are copied into ``utmp``. After that, the values of ``u`` are overwritten by
# evaluating the expression ``cppcode_unwrap_phi``.
#
# Note that applying ``cppcode_unwrap_phi`` to ``u`` and not to its copy ``utmp``
# would not work because it would at the same time overwrite the content of ``u``
# while looping in ``interpolate``. ::

class SwitchNodes:
    def __init__(self, u):

        self.u = u
        self.utmp = Function(u.function_space())
        self.unwrap_nodes = CompiledExpression(compile_cpp_code(RotVector.cppcode_unwrap_phi).unwrap_phi(), 
                                       element=u.function_space().ufl_element())
        self.unwrap_nodes.phi_function = self.utmp.cpp_object()

    def Switch(self):
        self.utmp.vector()[:] = self.u.vector()
        self.u.interpolate(self.unwrap_nodes)


swe = SwitchNodes(u)

# The Dirichelet boundary condition is a clamp at the origin: 
# both the displacement and the rotation are constrained to be zero. ::

left_displ = DirichletBC(UNKS_SPACE.sub(0), (0,0,0), "fabs(x[0]) <= DOLFIN_EPS")
left_rot = DirichletBC(UNKS_SPACE.sub(1), (0,0,0), "fabs(x[0]) <= DOLFIN_EPS")
bc = [left_displ, left_rot]

# A concentrated load is applied at the other end of the beam.
# To this end, the ``Right`` class is defined; its ``inside``
# method should return ``True`` only fo a point on the boundary
# and with the :math:`x_0` coordinate equal to the beam lenght. ::

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], beam_len)
right = Right()

# Since facets are actually points for an ``IntervalMesh`` a ``FacetFunction`` 
# is used to mark the end of the beam and define the ``Measure`` ``ds``: ::

boundaries = MeshFunction('size_t', mesh, mesh.topology().dim()-1)
boundaries.set_all(0)
right.mark(boundaries, 1)
ds = Measure('ds', domain=mesh, subdomain_data=boundaries)
dx = Measure('dx', domain=mesh)

# Define the rotation tensor ``alpha`` and the rotation differential tensor ``Gamma``
# as a function of the the rotation vector ``u_phi``: ::

alpha = RotVector.RotTensor(u_phi)
Gamma = RotVector.RotDiffTensor(u_phi)

# Define the linear and angular strains ``epsilon`` and ``beta``, i.e.
# :math:`\boldsymbol{\varepsilon} =
# \boldsymbol{\alpha}'^T\boldsymbol{x}'_{/s}-
# \boldsymbol{\alpha}^T\boldsymbol{x}'_{/s}` 
# and
# :math:`\boldsymbol{\beta} =
# \boldsymbol{\varPhi}^T\mathrm{ax}(\boldsymbol{\varPhi}_{/s}\boldsymbol{\varPhi}^T)`:
# ::

epsilon = dot(alpha.T, grad(u_u)[:, 0] + as_vector([1., 0., 0.])) - as_vector([1., 0., 0.])
beta = as_vector(dot(Gamma.T, grad(u_phi))[:,0])

# Define the finale values of the ditrubuted force and coule
# per unit of beam lenght and of the concentrated force and couple applied at the beam end ::

distributed_final_force = Constant([0., 0., 0.])
distributed_final_moment = Constant([0., 0., 0.])
concentrated_final_force = Constant([0., 0., 50.*0.0001])
concentrated_final_moment = Constant([0., 0., 200. * pi * 0.0001])

# the current value of the loads ::

distributed_force = Constant([0., 0., 0.])
distributed_moment = Constant([0., 0., 0.])
concentrated_force = Constant((0., 0., 0.))
concentrated_moment = Constant([0., 0., 0.])

# and the four second order constant tensors
# :math:`\overline{\boldsymbol{K}}_{\boldsymbol{\varepsilon}\boldsymbol{\varepsilon}}`,
# :math:`\overline{\boldsymbol{K}}_{\boldsymbol{\varepsilon}\boldsymbol{\beta}}`,
# :math:`\overline{\boldsymbol{K}}_{\boldsymbol{\beta}\boldsymbol{\varepsilon}}` and
# :math:`\overline{\boldsymbol{K}}_{\boldsymbol{\beta}\boldsymbol{\beta}}`
# defining the beam section linear constitutive law ::

# k_ee = Constant([[1.E4, 0., 0.], [0., 1.E4, 0.],[0., 0., 1.E4]])
# k_eb = Constant([[0., 0., 0.], [0., 0., 0.],[0., 0., 0.]])
# k_bb = Constant([[1.E2, 0., 0.], [0., 1.E2, 0.],[0., 0., 1.E2]])
# k_be = k_eb.T
# 
# c_ee = Constant([[1.E-4, 0., 0.], [0., 1.E-4, 0.],[0., 0., 1.E-4]])
# c_eb = Constant([[0., 0., 0.], [0., 0., 0.],[0., 0., 0.]])
# c_bb = Constant([[1.E-2, 0., 0.], [0., 1.E-2, 0.],[0., 0., 1.E-2]])
# c_be = k_eb.T

# The internal actions :math:`\boldsymbol{T}` and :math:`\boldsymbol{M}`
# are readily computed as ::

#delta_v  = inner(vu_T, dot(c_ee, u_T) + dot(c_eb, u_M))
#delta_v += inner(vu_M, dot(c_be, u_T) + dot(c_bb, u_M))

delta_v_expr = delta_v_expression(u)
merged_internal_actions_V = as_vector([vu_T[0], vu_T[1], vu_T[2], vu_M[0], vu_M[1], vu_M[2]])
merged_internal_actions_T = as_vector([du_T[0], du_T[1], du_T[2], du_M[0], du_M[1], du_M[2]])
delta_v = inner(merged_internal_actions_V, delta_v_expr)

de_delta_v_expr = de_delta_v_expression(u)
#pippo = dot(de_delta_v_expr, merged_internal_actions_T)
#print(pippo.ufl_shape)
de_delta_v = inner(merged_internal_actions_V, dot(de_delta_v_expr, merged_internal_actions_T))

#traction = dot(k_ee, epsilon) + dot(k_eb, beta)
#moment = dot(k_be, epsilon) + dot(k_bb, beta)

# The variation :math:`\delta\boldsymbol{\varepsilon}` and  :math:`\delta\boldsymbol{\beta}`
# of the strain vectors :math:`\boldsymbol{\varepsilon}` and :math:`\boldsymbol{\beta}`
# can be computed as ::

delta_epsilon = derivative(epsilon, u, v)
delta_beta = derivative(beta, u, v)

# Finally, the expresison of the functional 
# :math:`\int_L(\delta\boldsymbol{\varepsilon}\boldsymbol{T}+\delta\boldsymbol{\beta}\boldsymbol{M})\mathrm{d}s-  \int_L (\delta\boldsymbol{x}'\boldsymbol{t} + \boldsymbol{\varphi}_\delta\boldsymbol{m}) \mathrm{d}s - \sum_i\delta\boldsymbol{x}'_i\boldsymbol{F}_i - \boldsymbol{\varphi}_{\delta i}\boldsymbol{C}_i`
# is easily written ::

functional = inner(delta_epsilon, u_T) * dx + \
        inner(delta_beta, u_M) * dx +\
        inner(vu_T, epsilon) * dx +\
        inner(vu_M, beta) * dx -\
        delta_v * dx - \
        inner(v_u, distributed_force) * dx - \
        inner(v_phi, dot(Gamma.T, distributed_moment)) * dx - \
        inner(v_u, concentrated_force) * ds(1) - \
        inner(v_phi, dot(Gamma.T, concentrated_moment)) * ds(1)

# and the Jacobian matrix is defined by deriving it with respect to the unknown ``Function`` ``u`` ::

J = derivative(functional, u, du) - de_delta_v * dx

# The optput will be written to two files, the first for the displacement field (``u.sub(0)``)
# and the second for the rotation field (``u.sub(1)``). The undeformed configuration, for time equal to 0,
# is written immediately ::

file_res = XDMFFile('beam_result.xdmf')
file_res.parameters['functions_share_mesh'] = True
file_res.parameters['rewrite_function_mesh'] = False
file_res.parameters["flush_output"] = True

file_res.write(u.sub(0), t=0.)
file_res.write(u.sub(1), t=0.)
file_res.write(u.sub(2), t=0.)
file_res.write(u.sub(3), t=0.)

# As customary in nonlinear structural analysis the load is divided into ``num_incs`` load
# increments. ::

num_incs = 200
for i in range(1, num_incs + 1, 1):
    print('=====================')
    print('Load fraction ', 1. / num_incs * i)
    print('=====================')

# For each of them the corresponding loads are first computed ::

    distributed_force.assign(Constant(distributed_final_force.values() / num_incs * i))
    distributed_moment.assign(Constant(distributed_final_moment.values() / num_incs * i))
    concentrated_force.assign(Constant(concentrated_final_force.values() / num_incs * i))
    concentrated_moment.assign(Constant(concentrated_final_moment.values() / num_incs * i))

# Then, the residual ``-functional`` and the Jacobian matrix ``J`` are assembled together
# using the newly defined ``r_assemble_system`` method. The third argument, ``u.sub(1)``
# specifies which subspace defines the rotation field ::

    A, b = r_assemble_system(J, -functional, u.sub(1))
    fileout.write('\n')
    fileout.flush()

# After assembly, the boudary conditions can be applied. The nonlinear problem 
# can be solved by iterating until the residual norm is small enough::

    [bcs.apply(A, b) for bcs in bc]
    iteration = 0
    resnorm = b.norm('l2')
    print('Iteration = ', iteration, '\tResNorm = ', resnorm)
    while resnorm > 1.E-6:

# Within each iteration, solve the linear system and put the result into ``uinc``; after that, increment the solution ``u`` ::

        solve(A, uinc.vector(), b)
        u.vector()[:] += uinc.vector()

# Immediately after computing a new configuration call ``swe.Switch()``, so that the
# rotation vector magnitude is kept under :math:`\pi` ::

        swe.Switch()

# Assemble the Jacobian matrix and residual vector in the new configuration, 
# apply the boundary conditions, and compute the residual norm that will 
# be checked in the ``while`` loop test for convergence ::

        A, b = r_assemble_system(J, -functional, u.sub(1))
        fileout.write('\n')
        fileout.flush()
        [bcs.apply(A, b) for bcs in bc]
        resnorm = b.norm('l2')
        iteration += 1
        print('Iteration = ', iteration, '\tResNorm = ', resnorm)

# After convergence of the load step write the solution at time ``float(i) / num_incs``

    file_res.write(u.sub(0), t=float(i) / num_incs)
    file_res.write(u.sub(1), t=float(i) / num_incs)
    file_res.write(u.sub(2), t=float(i) / num_incs)
    file_res.write(u.sub(3), t=float(i) / num_incs)

