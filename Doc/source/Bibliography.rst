.. _bibliography:

Bibliography
============

.. _b1: 
   
   [1] Teodoro Merlini and Marco Morandini, 
   ''On successive differentiations of the rotation tensor: 
   An application to nonlinear beam elements'', 
   Journal of Mechanics of Materials and Structures, 
   Vol. 8, Number 5-7, 2013, pp 305-340, ISSN 1559-3959, 
   `http://dx.doi.org/10.2140/jomms.2013.8.305 <http://dx.doi.org/10.2140/jomms.2013.8.305>`_.


.. _b2: 

   [2] Manuel Ritto-Corrêa and David Camotim, 
   ''On the differentiation of the Rodrigues formula 
   and its significance for the vector-like parameterization 
   of Reissner-Simo beam theory'', 
   International Journal for Numerical Methods in Engineering, 
   Vol. 55, 2002, ISSN 1097-0207,
   `http://dx.doi.org/10.1002/nme.532 <http://dx.doi.org/10.1002/nme.532>`_.


.. _b3: 

   [3] Teodoro Merlini and Marco Morandini, 
   ''The helicoidal modeling in computational finite elasticity. 
   Part II - Multiplicative interpolation'', 
   Int. J. Solids and Structures, N. 18-19, Vol 41, 2004, pp. 5383-5409, ISSN 1559-3959, 
   `http://dx.doi.org/doi:10.1016/j.ijsolstr.2004.02.026 <http://dx.doi.org/doi:10.1016/j.ijsolstr.2004.02.026>`_. 

.. _b4: 

   [4] Nathan R. Barton, Paul R. Dawson, 
   ''A methodology for determining average lattice orientation 
   and its application to the characterization of grain substructure'', 
   Metall and Mat Trans A., Vol. 32, 2001, pp. 1967-1975, ISSN 1073-5623,
   `doi:10.1007/s11661-001-0009-x <http://dx.doi.org/10.1007/s11661-001-0009-x>`_.

.. _b5:
   
   [5] Adnan Ibrahimbegović, François Frey and Ivica Kožar,
   ''Computational aspects of vector-like parametrization of three-dimensional finite rotations''
   International Journal for Numerical Methods in Engineering,
   Vol. 38, Number 21, 1995, pp 3653-3673, ISSN 0029-5981, 
   `doi::10.1002/nme.1620382107 <http://doi.org/10.1002/nme.1620382107>`_.
