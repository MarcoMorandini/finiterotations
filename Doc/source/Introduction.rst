.. _introduction:

Introduction
============

The difficulty of handling finite rotations in Dolfin comes from the fact that
Dolfin, just like most of the finite element codes, cannot approximate a field of 
tensors :math:`\in\mathrm{SO}(3)` with an interpolation scheme
that respects the peculiar properties of the special orthogonal group they
belong to (see [:ref:`1 <b1>`, :ref:`3 <b3>`, :ref:`4 <b4>`] for details). 

One of the more popular solution to this problem is to resort to
a parametrization of the rotation field and interpolate the corresponding parameters.
However, no parametrization of an orthogonal tensor is bijective and
free from singularities; thus, one need to somehow "stay away" from the singularity.

On one hand, this seems to be exactly what a tool like Dolfin is not suited for: special cases
and numerical tricks scattered around in the code.
On the other hand, Dolfin's automatic differentiation technique greatly simplify the implementation, as the complex derivation of the beam tangent matrix is completely automatic. 
See [:ref:`1 <b1>`, :ref:`2 <b2>`] for a more detailed description of the beam equations
and for a thorough discussion of the really cumbersome manual 
linearization procedure that would be otherwise required.
