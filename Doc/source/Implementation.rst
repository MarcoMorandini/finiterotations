.. _implementation:

Implementation
==============

The last version of Dolfin, UFL, FIAT and FFC are required for a working implementation.
After installing UFL one should check that the variable ``CONDITIONAL_WORKAROUND`` defined into
``ufl/algorithms/apply_derivatives.py`` is set to ``False``: ::

  CONDITIONAL_WORKAROUND = False

This is required for a proper handling of conditionals, that are extensively used, see UFL's
bug `#70 <https://bitbucket.org/fenics-project/ufl/issues/70/derivative-of-conditional>`_ 
for details. One needs also to select the ``uflacs`` form compiler, because
the bug is not fixed with plain ``ffc``.

Should you see ``NaN``\s or ``Inf``\s popping out at the very first iteration this could be a
symptom of the fact that, in your installation, ``CONDITIONAL_WORKAROUND`` is set to ``True``.

.. toctree::
   :maxdepth: 2

   RotVector.py
   RotCoef.py
   Assembly
