.. _main:

Finite Rotation handling in Dolfin
==================================

This document describes how three dimensional finite rotations can be handled in Dolfin.
Finite rotations are of interest in structural computational mechanics when dealing with
Timoshenko beams and Mindlin shells with the so-called drilling degree of freedoms.

The idea is tested by developing a simple nonlinear beam model. 

By using the provided library the implementation of Mindlin shell 
models with drilling degree of freedoms
should be rather straighforward. Note however that alleviating shell shear locking would not be as easy as here with beams.

Besides how to handle with finite rotations, this document explains the following techniques

* use of ``conditional``\s in UFL expressions
* custom ``Assembly`` procedures
* compiled extension to the Dolfin python interface

A few solution steps of this demo, where a beam is loaded by a force and a
coaxial torque at its free end, will look like
 
.. image:: BeamRollup.png
   :scale: 25 %
   :align: center

You can also watch an  |animation| of the whole simulation, with the beam completely rolling up 10 times.

.. |animation| image:: WrenchRollup.ogv
   :scale: 50 %


   
.. toctree::
   :maxdepth: 2
   
   Introduction
   WeakForm
   Implementation
   beam.py
   Bibliography


Search
==================

* :ref:`search`


