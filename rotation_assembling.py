# -*- coding: utf-8 -*-
"""This module provides functionality for form assembly in Python,
corresponding to the C++ assembly and PDE classes.

The C++ :py:class:`assemble <dolfin.cpp.assemble>` function
(renamed to cpp_assemble) is wrapped with an additional
preprocessing step where code is generated using the
FFC JIT compiler.

The C++ PDE classes are reimplemented in Python since the C++ classes
rely on the dolfin::Form class which is not used on the Python side."""

# Copyright (C) 2007-2015 Anders Logg
#
# This file is part of DOLFIN.
#
# DOLFIN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DOLFIN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
#
# Modified by Martin Sandve Alnæs 2008-2015
# Modified by Johan Hake 2008-2009
# Modified by Garth N. Wells 2008-2013
# Modified by Joachim B. Haga 2012

import types
import os

# UFL modules
import ufl

# Import SWIG-generated extension module (DOLFIN C++)
import dolfin.cpp as cpp

# Local imports
from dolfin.fem.form import *
from dolfin import parameters

from dolfin import compile_cpp_code
from dolfin.fem.assembling import _create_dolfin_form, _create_tensor



pwd = os.path.dirname(os.path.abspath(__file__))
with open(pwd + "/RotationAssembler.cpp", "r") as f:
    rotation_assembler_code = f.read()
rotation_assembler = compile_cpp_code(rotation_assembler_code)

pwd = os.path.dirname(os.path.abspath(__file__))
with open(pwd + "/RotationAssemblerSystem.cpp", "r") as f:
    rotation_assembler_system_code = f.read()
rotation_assembler_system = compile_cpp_code(rotation_assembler_system_code)


__all__ = ["r_assemble", "r_assemble_system"]



# JIT assembler
def r_assemble(form,
             rotation_function,
             tensor=None,
             form_compiler_parameters=None,
             add_values=False,
             finalize_tensor=True,
             keep_diagonal=False,
             backend=None):
    """Assemble the given form and return the corresponding tensor.

    *Arguments*
        Depending on the input form, which may be a functional, linear
        form, bilinear form or higher rank form, a scalar value, a vector,
        a matrix or a higher rank tensor is returned.

    In the simplest case, no additional arguments are needed. However,
    additional arguments may and must in some cases be provided as
    outlined below.

    The ``form`` can be either a UFL Form or a DOLFIN Form.

    If the form defines integrals over different subdomains,
    :py:class:`MeshFunctions <dolfin.cpp.MeshFunction>` over the
    corresponding topological entities defining the subdomains can be
    provided.

    The implementation of the returned tensor is determined by the
    default linear algebra backend. This can be overridden by
    specifying a different backend.

    Each call to assemble() will create a new tensor. If the
    ``tensor`` argument is provided, this will be used instead.
    Sparsity pattern of ``tensor`` is reset iff ``tensor.empty()``
    holds.

    If the ``keep_diagonal`` is set to True, assembler ensures that
    potential zeros on a matrix diagonal are kept in sparsity pattern
    so every diagonal entry can be changed in a future (for example by
    ident() or ident_zeros()).

    Specific form compiler parameters can be provided by the
    ``form_compiler_parameters`` argument. Form compiler parameters
    can also be controlled using the global parameters stored in
    parameters["form_compiler"].

    *Examples of usage*
        The standard stiffness matrix ``A`` and a load vector ``b``
        can be assembled as follows:

        .. code-block:: python

            A = assemble(inner(grad(u),grad(v))*dx)
            b = assemble(f*v*dx)

        To prescribe the domains over which integrals will be
        evaluated, create a Measure with the MeshFunction passed as
        subdomain_data.  For instance, using a mesh function marking
        parts of the boundary:

        .. code-block:: python

            # MeshFunction marking boundary parts
            boundary_markers = FacetFunction("size_t", mesh)
            # ... fill values in boundary_markers

            # Measures with references to cell and boundary markers
            ds = Measure("ds", subdomain_data=boundary_markers)

            # Sample variational forms
            a = inner(grad(u), grad(v))*dx + p*u*v*ds(0)
            L = f*v*dx - g*v*ds(1) + p*q*v*ds(0)

            A = assemble(a)
            b = assemble(L)

        For interior facet integrals (dS), cell markers can be used to
        specify which cell is '+' and which is '-'. The '+' and '-'
        sides are chosen such that the cell marker value in the cell
        at the '+' side cell is larger than the cell marker value in
        the cell at the '-' side cell. If the values are equal or the
        cell markers are not provided, the sides are chosen
        arbitrarily.

        Note that currently, cell markers must be associated with a
        cell type integral (dx), and if you don't have such an
        integral a workaround is to add the integral of something over
        an empty domain such as 'f*dx(99)' with 99 a number not
        occuring among the cell markers. A better interface to handle
        this case will be provided later.

        .. code-block:: python

            # MeshFunctions marking boundary and cell parts
            boundary_markers = FacetFunction("size_t", mesh)
            cell_markers = CellFunction("size_t", mesh)
            # ... fill values in boundary_markers

            # Measures with references to cell and boundary markers
            ds = Measure("ds", domain=mesh, subdomain_data=boundary_markers)
            dx = Measure("dx", domain=mesh, subdomain_data=cell_markers)

            # Sample variational forms
            a = inner(grad(u), grad(v))*dx + p*u*v*ds(0)
            L = v*dx(99) - g*v*ds(1) + p*q*v*ds(0)

            A = assemble(a)
            b = assemble(L)

        To ensure that the assembled matrix has the right type, one may use
        the ``tensor`` argument:

        .. code-block:: python

            A = PETScMatrix()
            assemble(a, tensor=A)

        The form ``a`` is now assembled into the PETScMatrix ``A``.

    """

    # Create dolfin Form object referencing all data needed by assembler
    dolfin_form = _create_dolfin_form(form, form_compiler_parameters)

    # Create tensor
    comm = dolfin_form.mesh().mpi_comm()
    tensor = _create_tensor(comm, form, dolfin_form.rank(), backend, tensor)

    # Call C++ assemble function
    assembler = rotation_assembler.RotationAssembler()
    assembler.add_values = add_values
    assembler.finalize_tensor = finalize_tensor
    assembler.keep_diagonal = keep_diagonal
    assembler.assemble(tensor, dolfin_form, rotation_function.cpp_object())

    # Convert to float for scalars
    if dolfin_form.rank() == 0:
        tensor = tensor.get_scalar_value()

    # Return value
    return tensor

def r_assemble_system(forma, formb,
             rotation_function,
             tensora=None,
             tensorb=None,
             form_compiler_parameters=None,
             add_values=False,
             finalize_tensor=True,
             keep_diagonal=False,
             backend=None):
    """Assemble the given form and return the corresponding tensor.

    *Arguments*
        Depending on the input form, which may be a functional, linear
        form, bilinear form or higher rank form, a scalar value, a vector,
        a matrix or a higher rank tensor is returned.

    In the simplest case, no additional arguments are needed. However,
    additional arguments may and must in some cases be provided as
    outlined below.

    The ``form`` can be either a UFL Form or a DOLFIN Form.

    If the form defines integrals over different subdomains,
    :py:class:`MeshFunctions <dolfin.cpp.MeshFunction>` over the
    corresponding topological entities defining the subdomains can be
    provided.

    The implementation of the returned tensor is determined by the
    default linear algebra backend. This can be overridden by
    specifying a different backend.

    Each call to assemble() will create a new tensor. If the
    ``tensor`` argument is provided, this will be used instead.
    Sparsity pattern of ``tensor`` is reset iff ``tensor.empty()``
    holds.

    If the ``keep_diagonal`` is set to True, assembler ensures that
    potential zeros on a matrix diagonal are kept in sparsity pattern
    so every diagonal entry can be changed in a future (for example by
    ident() or ident_zeros()).

    Specific form compiler parameters can be provided by the
    ``form_compiler_parameters`` argument. Form compiler parameters
    can also be controlled using the global parameters stored in
    parameters["form_compiler"].

    *Examples of usage*
        The standard stiffness matrix ``A`` and a load vector ``b``
        can be assembled as follows:

        .. code-block:: python

            A = assemble(inner(grad(u),grad(v))*dx)
            b = assemble(f*v*dx)

        To prescribe the domains over which integrals will be
        evaluated, create a Measure with the MeshFunction passed as
        subdomain_data.  For instance, using a mesh function marking
        parts of the boundary:

        .. code-block:: python

            # MeshFunction marking boundary parts
            boundary_markers = FacetFunction("size_t", mesh)
            # ... fill values in boundary_markers

            # Measures with references to cell and boundary markers
            ds = Measure("ds", subdomain_data=boundary_markers)

            # Sample variational forms
            a = inner(grad(u), grad(v))*dx + p*u*v*ds(0)
            L = f*v*dx - g*v*ds(1) + p*q*v*ds(0)

            A = assemble(a)
            b = assemble(L)

        For interior facet integrals (dS), cell markers can be used to
        specify which cell is '+' and which is '-'. The '+' and '-'
        sides are chosen such that the cell marker value in the cell
        at the '+' side cell is larger than the cell marker value in
        the cell at the '-' side cell. If the values are equal or the
        cell markers are not provided, the sides are chosen
        arbitrarily.

        Note that currently, cell markers must be associated with a
        cell type integral (dx), and if you don't have such an
        integral a workaround is to add the integral of something over
        an empty domain such as 'f*dx(99)' with 99 a number not
        occuring among the cell markers. A better interface to handle
        this case will be provided later.

        .. code-block:: python

            # MeshFunctions marking boundary and cell parts
            boundary_markers = FacetFunction("size_t", mesh)
            cell_markers = CellFunction("size_t", mesh)
            # ... fill values in boundary_markers

            # Measures with references to cell and boundary markers
            ds = Measure("ds", domain=mesh, subdomain_data=boundary_markers)
            dx = Measure("dx", domain=mesh, subdomain_data=cell_markers)

            # Sample variational forms
            a = inner(grad(u), grad(v))*dx + p*u*v*ds(0)
            L = v*dx(99) - g*v*ds(1) + p*q*v*ds(0)

            A = assemble(a)
            b = assemble(L)

        To ensure that the assembled matrix has the right type, one may use
        the ``tensor`` argument:

        .. code-block:: python

            A = PETScMatrix()
            assemble(a, tensor=A)

        The form ``a`` is now assembled into the PETScMatrix ``A``.

    """

    # Create dolfin Form object referencing all data needed by assembler
    dolfin_forma = _create_dolfin_form(forma, form_compiler_parameters)
    dolfin_formb = _create_dolfin_form(formb, form_compiler_parameters)

    # Create tensor
    comma = dolfin_forma.mesh().mpi_comm()
    commb = dolfin_formb.mesh().mpi_comm()
    tensora = _create_tensor(comma, forma, dolfin_forma.rank(), backend, tensora)
    tensorb = _create_tensor(commb, formb, dolfin_formb.rank(), backend, tensorb)

    # Call C++ assemble function
    assembler = rotation_assembler_system.RotationAssemblerSystem()
    assembler.add_values = add_values
    assembler.finalize_tensor = finalize_tensor
    assembler.keep_diagonal = keep_diagonal
    assembler.assemble(tensora, dolfin_forma, tensorb, dolfin_formb, rotation_function.cpp_object())

    # Return value
    return (tensora, tensorb)

