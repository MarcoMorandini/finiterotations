#include <dolfin/mesh/Cell.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/fem/GenericDofMap.h>

namespace dolfin {

void MarkSwitchedCells (CellFunction<std::size_t>& sub_domains, 
                        const Function& dg0_cell_function,
                        std::size_t mark_value)
{
    std::shared_ptr<const Mesh> mesh = dg0_cell_function.function_space()->mesh();
    std::shared_ptr<const GenericDofMap> dofmap = dg0_cell_function.function_space()->dofmap();
    std::shared_ptr<const GenericVector> vector = dg0_cell_function.vector();
    dolfin_assert (dg0_cell_function.function_space()->dim() == 1);
    for (CellIterator cell(*mesh); !cell.end(); ++cell) {
        if (vector->operator[](dofmap->cell_dofs(cell->index())[0])) 
            sub_domains[cell->index()] = mark_value;
    }
}

}
